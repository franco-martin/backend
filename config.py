import os


class Config(object):
    GITLAB_TOKEN = os.environ.get('GITLAB_TOKEN')
    REDIS_HOST = os.environ.get('REDIS_HOST')
    ENABLE_APM = os.environ.get('ENABLE_APM', default=False)
