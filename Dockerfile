FROM python:3.9.1-alpine3.12
# Optimization for dependencies
COPY requirements.txt .
RUN pip install -r requirements.txt

ENV PORT=8080 \
    FLASK_APP="/app/main.py" \
    FLASK_RUN_HOST="0.0.0.0" \
    FLASK_RUN_PORT=8080
EXPOSE $PORT

COPY . .
USER root


# TBD 
#RUN chgrp -R 0 .  && chmod -R g+rwx .
#USER 1001

CMD ["sh","-c", "flask run" ]
