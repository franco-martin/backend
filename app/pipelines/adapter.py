from app.main import gl
from elasticapm import capture_span


def get_pipelines():
    if gl is not None:
        try:
            frontend = gl.projects.get(23070649)
            backend = gl.projects.get(23071207)
            response = {
                'pipelines': {
                    'frontend': {
                        'latest_id': frontend.pipelines.list()[0].id
                    },
                    'backend': {
                        'latest_id': backend.pipelines.list()[0].id
                    }
                },
                'status': 'ok'
            }
        except Exception as err:
            response = {
                'status': 'error',
                'message': 'there was an error in the backend'
            }
            print(str(err))
    else:
        response = {
            'status': 'error',
            'message': 'gitlab is not initialized'
        }
    return response
