import gitlab
import json
from flask_cors import CORS
from flask import Flask, logging
from config import Config
from app.redis.redis import RedisPersistence
from elasticapm.contrib.flask import ElasticAPM
from elasticapm.handlers.logging import LoggingHandler

from elasticapm import capture_span


app = Flask(__name__)
app.config.from_object(Config)
app.config['ELASTIC_APM'] = {
        'SERVICE_NAME': 'backend',
        'SECRET_TOKEN': '',
        'SERVER_URL': 'http://apm.elk.svc.cluster.local:8200',
        'TRANSACTIONS_IGNORE_PATTERNS': [
            '^GET \/healthcheck$'
        ]
    }
if app.config['ENABLE_APM']:
    apm = ElasticAPM(app)
    if __name__ == '__main__':
        # Create a logging handler and attach it.
        handler = LoggingHandler(client=apm.client)
        handler.setLevel(logging.WARN)
        app.logger.addHandler(handler)

CORS(app)
CORS(app, resources={r"/*": {"origins": "*"}})
redis = RedisPersistence(app.config['REDIS_HOST'], app)
app.redis = redis
try:
    gl = gitlab.Gitlab('https://gitlab.com', private_token=app.config['GITLAB_TOKEN'])
except Exception:
    gl = None


@app.route('/healthcheck')
def healthcheck():
    app.redis.health(),
    gl.projects.list(as_list=False)
    return "ok"


@app.route('/redeploy')
def redeploy():
    if not app.redis.redeploy_is_blocked():
        from app.redeploy.adapter import redeploy_frontend
        response = redeploy_frontend()
        app.redis.block_redeploy()
    else:
        response = {
            'status': 'error',
            'message': 'redeploy is blocked, wait 5 mins'
        }
    return json.dumps(response), 429


@app.route('/canary')
def canary():
    if not app.redis.canary_is_blocked():
        from app.canary.adapter import start_canary
        response = start_canary()
        app.redis.block_canary()
    else:
        response = {
            'status': 'error',
            'message': 'canary is blocked, wait 5 mins'
        }
    return json.dumps(response), 429


@app.route('/pipelines')
def pipelines():
    from app.pipelines.adapter import get_pipelines
    response = get_pipelines()
    return json.dumps(response)
