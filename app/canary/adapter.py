from app.main import gl, app
from elasticapm import capture_span


def start_canary():
    if gl is not None:
        try:
            frontend = gl.projects.get(23107953)
            pipeline = frontend.pipelines.create({'ref': 'master'})
            response = {
                'pipeline': {
                    'id': pipeline.id
                },
                'status': 'ok'
            }
            app.redis.block_canary()
        except Exception as err:
            response = {
                'status': 'error',
                'message': 'there was an error in the backend'
            }
            print(str(err))
    else:
        response = {
            'status': 'error',
            'message': 'gitlab is not initialized'
        }
    return response
