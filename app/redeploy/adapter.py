from app.main import gl
from app.main import app
from elasticapm import capture_span


def redeploy_frontend():
    if gl is not None:
        try:
            frontend = gl.projects.get(23070649)
            pipeline = frontend.pipelines.create({'ref': 'master'})
            response = {
                'pipeline': {
                    'id': pipeline.id
                },
                'status': 'ok'
            }
            app.redis.block_redeploy()
        except Exception as err:
            response = {
                'status': 'error',
                'message': 'there was an error in the backend'
            }
            print(str(err))
    else:
        response = {
            'status': 'error',
            'message': 'gitlab is not initialized'
        }
    return response
