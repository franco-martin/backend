import redis
from elasticapm import capture_span


class RedisPersistence:
    def __init__(self, host, app):
        pool = redis.ConnectionPool(host=host,
                                    port=6379,
                                    db=0,
                                    encoding=u'utf-8',
                                    decode_responses=True)
        self.redis = redis.Redis(connection_pool=pool)
        self.app = app

    def health(self):
        self.redis.ping()
        return "ok"

    def block_redeploy(self):
        try:
            self.redis.set('redeploy_blocked', 'true', ex=3000)
        except Exception as err:
            print("Error blocking redeploy. Error is: {}".format(str(err)))

    def redeploy_is_blocked(self):
        try:
            if self.redis.get('redeploy_blocked') is None:
                return False
            else:
                return True
        except Exception as err:
            print("Error blocking redeploy. Error is: {}".format(str(err)))
            return True

    def block_canary(self):
        try:
            self.redis.set('canary_blocked', 'true', ex=3000)
        except Exception as err:
            print("Error blocking redeploy. Error is: {}".format(str(err)))

    def canary_is_blocked(self):
        try:
            if self.redis.get('canary_blocked') is None:
                return False
            else:
                return True
        except Exception as err:
            print("Error blocking canary. Error is: {}".format(str(err)))
            return True
